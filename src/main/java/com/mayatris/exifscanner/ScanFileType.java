package com.mayatris.exifscanner;

import com.google.common.collect.Sets;

import java.util.Set;

public enum ScanFileType {
    JPEG("JPEG", "JPG", "JIFF"),
    GARBAGE("DS_STORE", "JBF", "DB"),
    OTHER;

    private Set<String> extensions;

    ScanFileType(String... extensions) {
        this.extensions = Sets.newHashSet(extensions);
    }

    public static ScanFileType getType(String extension) {
        for (ScanFileType scanFileType : ScanFileType.values()) {
            if (scanFileType.equals(OTHER)) {
                continue;
            }
            if (scanFileType.extensions.contains(extension.toUpperCase())) {
                return scanFileType;
            }
        }
        return OTHER;
    }
}
