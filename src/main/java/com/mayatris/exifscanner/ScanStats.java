package com.mayatris.exifscanner;

public class ScanStats {
    private long totalSize = 0L;
    private long totalPictures = 0L;
    private long totalWithMetaData = 0L;
    private long totalWithThumbnails = 0L;

    public long getTotalSize() {
        return totalSize;
    }

    public long getTotalPictures() {
        return totalPictures;
    }

    public long getTotalWithMetaData() {
        return totalWithMetaData;
    }

    public long getTotalWithThumbnails() {
        return totalWithThumbnails;
    }

    public void addToTotalSize(long amount) {
        totalSize += amount;
    }

    public void addToTotalPictures(long amount) {
        totalPictures += amount;
    }

    public void addToTotalWithMetaData(long amount) {
        totalWithMetaData += amount;
    }

    public void addToTotalWithThumbnails(long amount) {
        totalWithThumbnails += amount;
    }

}
