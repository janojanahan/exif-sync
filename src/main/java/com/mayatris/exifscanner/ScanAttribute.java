package com.mayatris.exifscanner;

public enum ScanAttribute {
    METADATA, THUMBNAIL
}
