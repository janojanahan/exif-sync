package com.mayatris.exifscanner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.common.base.Splitter;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

import static com.mayatris.exifscanner.ScanAttribute.METADATA;
import static com.mayatris.exifscanner.ScanAttribute.THUMBNAIL;
import static java.util.logging.Level.INFO;
import static java.util.logging.Level.WARNING;
import static java.util.stream.Collectors.toCollection;

public class FilesystemGraph {
    private static final Logger LOGGER = Logger.getLogger(FilesystemGraph.class.getName());

    private final ObjectMapper objectMapper = new ObjectMapper();

    private FileItemRecord rootGraph = new FileItemRecord(new TreeMap<>());

    private int prunedFiles = 0;

    public FilesystemGraph() {
        SimpleModule module = new SimpleModule("basic");
        module.addAbstractTypeMapping(Map.class, TreeMap.class);
        objectMapper.registerModule(module);
    }

    public void loadTree(File file) throws IOException {
        LOGGER.log(INFO, "Reading previous graph file: " + file.getAbsolutePath());
        if (!file.exists() || !file.isFile()) {
            LOGGER.log(WARNING, "Missing previous graph file: " + file.getAbsolutePath());
            return;
        }
        rootGraph = objectMapper.readValue(file, FileItemRecord.class);
    }

    public void saveTree(File file) throws IOException {
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, rootGraph);
    }

    public FileItemRecord getFileInfo(Path path) {
        var relativePath = path.toString();
        var pathSegments = Splitter.on(FileSystems.getDefault().getSeparator())
                .trimResults()
                .splitToStream(relativePath)
                .collect(toCollection(LinkedList::new));
        var head = pathSegments.pollFirst();
        return _recurseFindAndBuild(head, pathSegments, rootGraph);

    }

    private FileItemRecord _recurseFindAndBuild(String head, LinkedList<String> pathSegments, FileItemRecord graph) {
        graph.setScanned(true);
        if (pathSegments.size() == 0) {
            var item = graph.getChildren().computeIfAbsent(head, s -> new FileItemRecord());
            item.setScanned(true);
            return item;
        } else {
            var fileItem = graph.getChildren().computeIfAbsent(head,
                    s -> new FileItemRecord(new TreeMap<>()));
            return _recurseFindAndBuild(pathSegments.pollFirst(), pathSegments, fileItem);
        }
    }

    public long pruneDeleted() {
        long prunedItems = _prune(rootGraph);
        LOGGER.log(INFO, "Number of pruned files or directories:" + prunedItems);
        return prunedItems;
    }

    private long _prune(FileItemRecord node) {
        long prunedItems = 0L;
        var itemsToPrune = new LinkedList<String>();
        for (var e : node.getChildren().entrySet()) {
            if (e.getValue().isScanned()) {
                if (e.getValue().isDirectory()) {
                    prunedItems += _prune(e.getValue());
                }
            } else {
                itemsToPrune.add(e.getKey());
            }
        }
        itemsToPrune.forEach(s -> node.getChildren().remove(s));
        return prunedItems + itemsToPrune.size();
    }

    public ScanStats getScanStats() {
        return getScanStats(rootGraph, new ScanStats());
    }

    private ScanStats getScanStats(FileItemRecord rootGraph, ScanStats scanStats) {
        for (var e : rootGraph.getChildren().entrySet()) {
            var item = e.getValue();
            if (item.isDirectory()) {
                getScanStats(item, scanStats);
            } else {
                scanStats.addToTotalPictures(1);
                scanStats.addToTotalSize(item.getFileSize());
                if (item.getScanAttributes().contains(METADATA)) {
                    scanStats.addToTotalWithMetaData(1);
                }
                if (item.getScanAttributes().contains(THUMBNAIL)) {
                    scanStats.addToTotalWithThumbnails(1);
                }
            }
        }
        return scanStats;
    }
}
