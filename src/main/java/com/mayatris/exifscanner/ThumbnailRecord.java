package com.mayatris.exifscanner;

import java.util.Comparator;
import java.util.Objects;

public class ThumbnailRecord implements Comparable<ThumbnailRecord>{
    private final String filePath;

    public ThumbnailRecord(String filePath) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ThumbnailRecord that = (ThumbnailRecord) o;
        return Objects.equals(filePath, that.filePath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(filePath);
    }

    @Override
    public int compareTo(ThumbnailRecord o) {
        return Objects.compare(this.filePath, o.filePath, Comparator.naturalOrder());
    }
}
