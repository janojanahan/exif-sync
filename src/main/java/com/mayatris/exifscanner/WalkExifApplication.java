package com.mayatris.exifscanner;

import com.google.common.collect.Multiset;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.logging.Logger;

import static java.util.Optional.ofNullable;
import static java.util.logging.Level.INFO;

public class WalkExifApplication {

    private static final Logger LOGGER = Logger.getLogger(WalkExifApplication.class.getName());
    private static final String DEFAULT_SCAN_DIR = ".";
    private static final BigDecimal BYTE_DIVISOR = new BigDecimal(1024);
    private static final DateTimeFormatter DATE_FILENAME = DateTimeFormatter.ofPattern("yyyy-MM-dd_HHmm");

    private static final FilesystemGraph filesystemGraph = new FilesystemGraph();
    private static String thumbsOutputLocation = null;
    private static String configFilename = "/var/exifscan/config.json";

    private static String parseCommandLine(String[] args) {
        thumbsOutputLocation = ofNullable(args).filter(a -> a.length > 1).map(a -> a[1]).orElse(null);
        return ofNullable(args).filter(a -> a.length > 0).map(a -> a[0]).orElse(DEFAULT_SCAN_DIR);
    }


    public static void main(String[] args) throws IOException {
        String sourceDirectory = parseCommandLine(args).replaceAll("\"", "");
        Path startPath = Paths.get(sourceDirectory);

        LOGGER.log(INFO, "scanning: " + startPath);

        File configFile = new File(configFilename);
        filesystemGraph.loadTree(configFile);
        ExifVisitor visitor = new ExifVisitor(startPath, filesystemGraph);

        Files.walkFileTree(startPath, visitor);
        var prunedFiles = filesystemGraph.pruneDeleted();
        var scanStats = filesystemGraph.getScanStats();
        var filesNoMetadata = scanStats.getTotalPictures() - scanStats.getTotalWithMetaData();

        LOGGER.log(INFO, "jpeg files found:" + scanStats.getTotalPictures());
        LOGGER.log(INFO, "jpeg files no metadata:" + filesNoMetadata);
        LOGGER.log(INFO, "jpeg files with metadata:" + scanStats.getTotalWithMetaData());
        LOGGER.log(INFO, "jpeg files adjusted:" + visitor.getDateAdjusted());
        LOGGER.log(INFO, "jpeg total size:" + sanitizeSizes(scanStats.getTotalSize()));
        LOGGER.log(INFO, "Pruned files from DB (deleted/moved):" + prunedFiles);
        LOGGER.log(INFO, "Files skipped due to no change:" + visitor.getFilesSkipped());
        printOtherFileTypes(visitor.getUnknownExtensions());
        printFilesWithThumbnails(visitor.getFilesWithThumbnails(), scanStats);
        filesystemGraph.saveTree(configFile);
    }

    private static void printFilesWithThumbnails(Set<ThumbnailRecord> filesWithThumbnails, ScanStats scanStats) throws IOException {
        LOGGER.log(INFO, filesWithThumbnails.size() + " files with thumbnails:");
        if (thumbsOutputLocation != null) {
            String filename = thumbsOutputLocation + "thumbnail_scan-" + DATE_FILENAME.format(LocalDateTime.now()) + ".csv";
            Path output = Paths.get(filename);
            try (PrintWriter pw = new PrintWriter(output.toFile())) {
                filesWithThumbnails.stream()
                        .map(ThumbnailRecord::getFilePath)
                        .forEach(pw::println);
            }
            LOGGER.log(INFO, "Written report to: " + filename);
        }
    }

    private static void printOtherFileTypes(Multiset<String> unknownExtensions) {
        unknownExtensions.entrySet().forEach(e -> LOGGER.log(INFO, e.getElement() + ':' + e.getCount()));
    }

    private static String sanitizeSizes(long number) {
        BigDecimal value = new BigDecimal(number);
        String unit = "B";
        if (value.longValue() > 1024) {
            value = value.divide(BYTE_DIVISOR, 2, RoundingMode.HALF_UP);
            unit = "KiB";
        }
        if (value.longValue() > 1024) {
            value = value.divide(BYTE_DIVISOR, 2, RoundingMode.HALF_UP);
            unit = "MiB";
        }
        return value.toString() + unit;
    }
}
