package com.mayatris.exifscanner;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.concurrent.atomic.AtomicInteger;

public class DirectoryRecord {
    private static final DecimalFormat FORMAT = new DecimalFormat("0.##");

    private final long startTime;
    private final AtomicInteger scannedFiles;
    private final AtomicInteger changedFiles;

    public DirectoryRecord() {
        this.startTime = System.currentTimeMillis();
        changedFiles = new AtomicInteger();
        scannedFiles = new AtomicInteger();
    }

    public String getEndTimeInSeconds() {
        return FORMAT.format((float)(System.currentTimeMillis() - startTime) / 1000) + "s";
    }

    public int getScannedFiles() {
        return scannedFiles.get();
    }

    public int getChangedFiles() {
        return changedFiles.get();
    }

    public boolean hasChangedFiles() {
        return getChangedFiles() > 0;
    }

    public void incrementChanged() {
        this.changedFiles.incrementAndGet();
    }

    public void incrementScanned() {
        this.scannedFiles.incrementAndGet();
    }
}
