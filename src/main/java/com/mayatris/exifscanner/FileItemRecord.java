package com.mayatris.exifscanner;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.EnumSet;
import java.util.Map;
import java.util.Objects;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileItemRecord {
    private Map<String, FileItemRecord> children;
    private long timeStamp;
    private long fileSize;
    private EnumSet<ScanAttribute> scanAttributes = EnumSet.noneOf(ScanAttribute.class);

    @JsonIgnore
    private boolean scanned = false;

    public FileItemRecord() {

    }

    public FileItemRecord(Map<String, FileItemRecord> children) {
        this.children = children;
    }

    public FileItemRecord(long timeStamp, long fileSize) {
        this.timeStamp = timeStamp;
        this.fileSize = fileSize;
        this.scanned = true;
    }

    public Map<String, FileItemRecord> getChildren() {
        return children;
    }

    public void setChildren(Map<String, FileItemRecord> children) {
        this.children = children;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public boolean isScanned() {
        return scanned;
    }

    public void setScanned(boolean scanned) {
        this.scanned = scanned;
    }

    public EnumSet<ScanAttribute> getScanAttributes() {
        return scanAttributes;
    }

    public void setScanAttributes(EnumSet<ScanAttribute> scanAttributes) {
        this.scanAttributes = scanAttributes;
    }

    @JsonIgnore
    public boolean isFile() {
        return children == null;
    }

    @JsonIgnore
    public boolean isDirectory() {
        return !isFile();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileItemRecord that = (FileItemRecord) o;
        return timeStamp == that.timeStamp && fileSize == that.fileSize && scanned == that.scanned && Objects.equals(children, that.children) && Objects.equals(scanAttributes, that.scanAttributes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(children, timeStamp, fileSize, scanAttributes, scanned);
    }

    @Override
    public String toString() {
        return "FileItemRecord{" +
                "children=" + children +
                ", timeStamp=" + timeStamp +
                ", fileSize=" + fileSize +
                ", scanAttributes=" + scanAttributes +
                ", scanned=" + scanned +
                '}';
    }
}
