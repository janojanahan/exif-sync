package com.mayatris.exifscanner;

import com.drew.imaging.jpeg.JpegMetadataReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.exif.ExifThumbnailDirectory;
import com.google.common.collect.Multiset;
import com.google.common.collect.TreeMultiset;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static com.mayatris.exifscanner.ScanAttribute.METADATA;
import static com.mayatris.exifscanner.ScanAttribute.THUMBNAIL;
import static java.lang.StrictMath.abs;
import static java.util.Optional.ofNullable;
import static java.util.logging.Level.INFO;

public class ExifVisitor extends SimpleFileVisitor<Path> {

    private static Logger LOGGER;

    static {
        InputStream stream = ExifVisitor.class.getClassLoader().
                getResourceAsStream("logging.properties");
        try {
            LogManager.getLogManager().readConfiguration(stream);
            LOGGER = Logger.getLogger(ExifVisitor.class.getName());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy:MM:dd HH:mm:ss");
    private final Path startPath;
    private final FilesystemGraph filesystemGraph;

    private long dateAdjusted = 0L;

    private long filesSkipped = 0L;

    private final Multiset<String> unknownExtensions = TreeMultiset.create();
    private final Map<String, DirectoryRecord> directories = new HashMap<>();

    private final Set<ThumbnailRecord> filesWithThumbnails = new TreeSet<>();

    public ExifVisitor(Path startPath, FilesystemGraph filesystemGraph) {
        this.startPath = startPath;
        this.filesystemGraph = filesystemGraph;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
            throws IOException {
        DirectoryRecord directoryRecord = ofNullable(directories.get(file.getParent().toString()))
                .orElseGet(DirectoryRecord::new);
        directoryRecord.incrementScanned();
        Optional<String> extensionOptional = getExtensionByStringHandling(file.getFileName().toString());
        if (extensionOptional.isEmpty()) {
            return FileVisitResult.CONTINUE;
        }
        String extension = extensionOptional.get().toUpperCase();
        ScanFileType type = ScanFileType.getType(extension);
        if (type.equals(ScanFileType.JPEG)) {
            extractMetaData(file, directoryRecord);
        } else if (type.equals(ScanFileType.GARBAGE)) {
            System.out.println("Deleting Garbage File: " + file.getFileName());
            Files.delete(file.toAbsolutePath());
            directoryRecord.incrementChanged();
        } else {
            unknownExtensions.add(extension);
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        String path = dir.toAbsolutePath().toString();
        directories.computeIfAbsent(path, (s -> new DirectoryRecord()));
        return super.preVisitDirectory(dir, attrs);
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        String path = dir.toAbsolutePath().toString();
        DirectoryRecord directoryRecord = directories.get(path);
        if (directoryRecord != null) {
            String output = Optional.of(directoryRecord)
                    .filter(DirectoryRecord::hasChangedFiles)
                    .map(d -> path + ". Changed:" + d.getChangedFiles() + " of " + d.getScannedFiles() + " files. " + d.getEndTimeInSeconds())
                    .orElseGet(() -> path + " Scanned " + directoryRecord.getScannedFiles() + ". " + directoryRecord.getEndTimeInSeconds());
            LOGGER.log(INFO, output);
        }
        return super.postVisitDirectory(dir, exc);
    }

    private void extractMetaData(Path path, DirectoryRecord directoryRecord) {
        Path relative = startPath.relativize(path);
        var fileItem = filesystemGraph.getFileInfo(relative);
        File file = path.toFile();

        try {
            BasicFileAttributeView attr = Files.getFileAttributeView(path, BasicFileAttributeView.class);
            Instant fileCreated = attr.readAttributes().creationTime().toInstant();
            Instant fileModified = attr.readAttributes().lastModifiedTime().toInstant();
            boolean createModifiedDiff = abs(Duration.between(fileCreated, fileModified).toMinutes()) > 5;
            long fileSize = file.length();
            long durationFromPrevious = abs(Duration.between(fileCreated, Instant.ofEpochSecond(fileItem.getTimeStamp())).toMinutes());
            final boolean processFurther = (durationFromPrevious > 5) || (fileSize != fileItem.getFileSize()) || createModifiedDiff;
            if (processFurther) {
                Instant exifDate = processExifAndGetDate(file, fileItem);
                if (exifDate != null) {
                    fileItem.getScanAttributes().add(METADATA);
                    long duration = abs(Duration.between(fileCreated, exifDate).toMinutes());
                    if (duration > 5 || createModifiedDiff) {
                        LOGGER.log(INFO, path.getFileName().toString() + " time: " + fileCreated.toString() + " -> " + exifDate);
                        FileTime newTime = FileTime.from(exifDate);
                        attr.setTimes(newTime, newTime, newTime);
                        dateAdjusted++;
                        directoryRecord.incrementChanged();
                    }
                } else {
                    if (createModifiedDiff) {
                        Instant earliestInstant = fileCreated.isBefore(fileModified) ? fileCreated : fileModified;
                        FileTime newTime = FileTime.from(earliestInstant);
                        attr.setTimes(newTime, newTime, newTime);
                        dateAdjusted++;
                        directoryRecord.incrementChanged();
                    }
                    fileItem.getScanAttributes().remove(METADATA);
                    fileItem.getScanAttributes().remove(THUMBNAIL);
                }
                fileItem.setFileSize(fileSize);
                attr = Files.getFileAttributeView(path, BasicFileAttributeView.class);
                fileCreated = attr.readAttributes().creationTime().toInstant();
                fileItem.setTimeStamp(fileCreated.getEpochSecond());
            } else {
                filesSkipped++;
                if (fileItem.getScanAttributes().contains(METADATA)) {
                    filesWithThumbnails.add(new ThumbnailRecord(file.getAbsolutePath()));
                }
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "IOError on file " + path.getFileName(), e);
        } catch (DateTimeParseException e) {
            LOGGER.log(Level.SEVERE, "unable to parse date on EXIF " + path.getFileName(), e);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "other exception " + path.getFileName(), e);
        }
    }

    private Optional<String> getExtensionByStringHandling(String filename) {
        return ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }

    private Instant parseTextToInstantLocalTZ(String dateString) {
        return LocalDateTime.parse(                   // Parse as an indeterminate `LocalDate`, devoid of time zone or offset-from-UTC. NOT a moment, NOT a point on the timeline.
                        dateString, formatter)                     // Returns a `LocalDateTime` object.
                .atZone(                               // Apply a zone to that unzoned `LocalDateTime`, giving it meaning, determining a point on the timeline.
                        ZoneId.of("Europe/London")     // Always specify a proper time zone with `Continent/Region` format, never a 3-4 letter pseudo-zone such as `PST`, `CST`, or `IST`.
                )                                      // Returns a `ZonedDateTime`. `toString` → 2018-05-12T16:30-04:00[America/Toronto].
                .toInstant();
    }

    private Instant processExifAndGetDate(File file, FileItemRecord fileItem) {

        fileItem.getScanAttributes().remove(THUMBNAIL);
        try {
            Metadata metadata = JpegMetadataReader.readMetadata(file);
            try {
                // check if it has a thumbnail
                ExifThumbnailDirectory thumbnailDirectory = metadata.getFirstDirectoryOfType(ExifThumbnailDirectory.class);
                if (thumbnailDirectory != null) {
                    long thumbLength = thumbnailDirectory.getLong(ExifThumbnailDirectory.TAG_THUMBNAIL_LENGTH);
                    if (thumbLength > 0) {
                        filesWithThumbnails.add(new ThumbnailRecord(file.getAbsolutePath()));
                        fileItem.getScanAttributes().add(THUMBNAIL);
                    }
                }
            } catch (Exception e) {
                System.out.println(file.getName() + " threw exception reading thumbnail: " + e.getMessage());
            }

            ExifSubIFDDirectory directory = metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
            String dateString = directory.getString(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL);

            return ofNullable(dateString)
                    .map(this::parseTextToInstantLocalTZ)
                    .orElse(null);
        } catch (Exception e) {
            return null;
        }
    }

    public Multiset<String> getUnknownExtensions() {
        return unknownExtensions;
    }

    public long getDateAdjusted() {
        return dateAdjusted;
    }

    public long getFilesSkipped() {
        return filesSkipped;
    }

    public Set<ThumbnailRecord> getFilesWithThumbnails() {
        return filesWithThumbnails;
    }
}
